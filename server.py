import asyncio
import math
from aiohttp import web

from stores.file_based import FileStore
from stores.errors import MissingUserError


class Server(object):
    def __init__(self, store):
        self.store = store

    def get_response(self, result=None, error=None, status=200):
        assert type(error) is str or error is None, "If present error must be a string"

        return web.json_response({
            "error": error,
            "result": result
        }, status=status)

    @staticmethod
    def get_pages(thing_to_page, page, size):
        """
        thing_to_page:
            - must implement the `__len__` method
            - class attribute list_url such that list_url.format(page=page, size=size)
              makes sense
        """
        page_url = lambda page: thing_to_page.list_url.format(page=page, size=size)

        last_page = math.floor(len(thing_to_page) / size)
        paging = {
            "first": page_url(1),
            "last": page_url(last_page),
        }
        if page < last_page:
            paging["next"] = page_url(page + 1)

        if page < last_page and page > 1:
                paging["previous"] = page_url(page - 1)
        return paging

    async def search(self, request):
        try:
            persona = self.store.search(request.match_info["username"])
        except MissingUserError as e:
            return self.get_response(error=str(e), status=404)
        else:
            return self.get_response(result=persona)

    async def delete(self, request):
        try:
            self.store.delete(request.match_info["username"])
        except MissingUserError as e:
            return self.get_response(error=str(e), status=404)
        else:
            return self.get_response(result="success")

    async def list(self, request):
        try:
            page = int(request.query.get("page", 1))
            size = int(request.query.get("size", 10))
        except ValueError:
            return self.get_response(error="page and size arguments must be integer")
        results = self.store.list(page, size)
        return self.get_response(result={
            "personas": results,
            "paging": self.get_pages(self.store, page, size)
        })

    async def index(self, request):
        return web.FileResponse("/app/src/index.html")

    @property
    def routes(self):
        return [
            web.get("/search/{username}/", self.search),
            web.get("/people/", self.list),
            web.delete("/people/{username}/", self.delete),

            # Ideally these would be behind a proper web server for serving
            # static files (e.g. nginx, apache)
            web.get("/", self.index),
            web.static("/", "/app/src/"),
        ]

    def run(self):
        middlewares = (web.normalize_path_middleware(
            append_slash=True, merge_slashes=True
        ),)
        app = web.Application(middlewares=middlewares)
        app.add_routes(self.routes)
        web.run_app(app)


def main():
    store = FileStore("/app/fake_profiles.json")
    server = Server(store)
    server.run()


if __name__ == "__main__":
    main()
