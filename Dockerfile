FROM alpine:latest

ADD . /app
RUN apk update && apk add python3 py3-virtualenv unzip
RUN cd /app && virtualenv env -p /usr/bin/python3 
RUN cd /app && source env/bin/activate && pip install -r requirements.txt
RUN cd /app && unzip fake_profiles.zip
