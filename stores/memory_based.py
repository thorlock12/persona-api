import math

from collections import OrderedDict
from stores.errors import MissingUserError


class MemoryStore(object):
    """
    Takes a dictionary of the personas
    """
    def __init__(self, data):
        self.data = OrderedDict({
            d["username"]: d for d in data
        })
        self._length = len(self.data)

    def delete(self, username):
        if username in self.data:
            del self.data[username]
            self._length -= 1

    def search(self, username):
        if username in self.data:
            return self.data[username]

        raise MissingUserError(username)

    def list(self, page=1, size=10):
        start_index = (page - 1) * size
        end_index = page * size

        return self.get_data()[start_index:end_index]

    def __len__(self):
        return self._length

    def get_data(self):
        return list(self.data.values())
