import json
from stores.memory_based import MemoryStore


class FileStore(object):
    list_url = "/people/?page={page}&size={size}"

    def __init__(self, path):
        with open(path, 'r') as f:
            data = json.loads(f.read())

        self._path = path
        self._memory_store = MemoryStore(data)

    def delete(self, username):
        self._memory_store.delete(username)
        self._write()

    def search(self, username):
        return self._memory_store.search(username)

    def list(self, page=1, size=10):
        return self._memory_store.list(page, size)

    def _write(self):
        with open(self._path, 'w') as f:
            f.write(json.dumps(self._memory_store.get_data()))

    def __len__(self):
        return len(self._memory_store)
