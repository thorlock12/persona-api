class MissingUserError(Exception):
    def __init__(self, username):
        self.username = username

    def __str__(self):
        return "{} does not exist!".format(self.username)
