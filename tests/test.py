import pytest
import random
import json
import os
import string
from contextlib import contextmanager

from stores.memory_based import MemoryStore
from stores.file_based import FileStore
from stores.errors import MissingUserError


@contextmanager
def write_temp_file(data):
    """
    Helper function to create a temporary file.
    Note: We can't use the tempfile package in the standard library because it
          disappears when closed.
    """
    def get_file_path():
        random_name = "".join(random.choices(string.ascii_letters, k=32))
        file_path = os.path.join("/tmp", random_name)
        return file_path

    # Generate a random name that is unique (the odds on it not being are pretty
    # low, but we must make sure!)
    file_path = get_file_path()
    while os.path.exists(file_path):
        file_path = get_file_path()

    # Open file and write to it
    with open(file_path, "w") as f:
        f.write(json.dumps(data))

    # yield that random name so the file can be reopened
    yield file_path

    # Delete that file for cleanup purposes
    os.remove(file_path)


def test_delete_memory_store():
    """
    Test deleting a persona stored in memory
    """
    store = MemoryStore([{"username": "a"}])
    assert len(store.data) == 1
    store.delete("a")
    assert len(store.data) == 0


def test_delete_missing_memory_store():
    """
    Test deleting a non-existant persona stored in memory
    """
    with pytest.raises(MissingUserError):
        store = MemoryStore([{"username": "a"}])
        store.delete("b")


def test_search_memory_store():
    """
    Test searching for a persona stored in memory
    """
    store = MemoryStore([{"username": "a"}])
    persona = store.search("a")
    assert persona == {"username": "a"}


def test_search_missing_memory_store():
    """
    Test searching for a non-existant persona stored in memory
    """
    with pytest.raises(MissingUserError):
        store = MemoryStore([{"username": "a"}])
        store.search("b")


def test_list_memory_store():
    """
    Test listing personas stored in memory, including pagination
    """
    store = MemoryStore([{"username": "a"}, {"username": "b"}])
    personas = store.list(page=1, size=1)
    assert len(personas) == 1
    assert personas[0]["username"] == "a"

    personas = store.list(page=2, size=1)
    assert len(personas) == 1
    assert personas[0]["username"] == "b"

    personas = store.list(page=1, size=10)
    assert len(personas) == 2


def test_file_based_store_creation():
    """
    Test that file based store reads a file correctly
    """
    with write_temp_file([{"username": "a"}]) as file_name:
        store = FileStore(file_name)
        persona = store.search("a")
        assert persona["username"] == "a"


def test_delete_file_based_store_creation():
    """
    Test that file based store deletes a persona from a file
    """
    with write_temp_file([{"username": "a"}]) as file_name:
        store1 = FileStore(file_name)
        persona = store1.delete("a")

        # We reopen the file to ensure it's actually been deleted there
        store2 = FileStore(file_name)
        with pytest.raises(MissingUserError):
            store2.search("a")
