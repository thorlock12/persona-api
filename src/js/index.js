// simple vue plugin that installs api accesss for all components
const Api = {
  search: function(username){
    return fetch(`/search/${username}/`)
      .then(r => r.json())
  },
  remove: function(username){
    request = new Request(`/people/${username}/`, {
      method: "DELETE",
    })
    return fetch(request)
      .then(r => r.json())
  },
  list: function(page, size){
    return fetch(`/people/?page=${page}&size=${size}`)
      .then(r => r.json())
  },
}

// Persona component handles display for each individual persona
const Persona = Vue.component("persona", {
    name: "persona",
    props: ["persona"],
    template: `
      <div class="persona">
          <button class="btn delete-btn" @click="$parent.remove(persona.username)">
            Delete
          </button>
          Username: {{ persona.username }}
      </div>
    `,
})

// Root component
const app = new Vue({
  el: "#app",
  data: {
    personas: [],
    page: 1,
    size: 10,
    error: null,
    username: null,
    paging: {},
  },
  mounted: function(){
    this.list()
  },
  computed: {
    showClear: function(){
      return this.error !== null || this.username !== null;
    },
    showPrevPage: function(){
      return this.page > 1 && this.username == null;
    },
    showNextPage: function(){
      return this.username === null && this.paging.next !== undefined;
    }
  },
  methods: {
    list: function(){
        Api.list(this.page, this.size)
          .then(response => {
            if(response.error !== null){
                this.handleError(response.error);
            } else {
                this.personas = response.result.personas;
                this.paging = response.result.paging;
                this.error = null;
            }
          })
    },
    search: function(username){
        Api.search(username)
          .then(response => {
            this.page = 1;
            if(response.error !== null){
                this.handleError(response.error);
            } else {
                this.personas = [response.result];
                this.error = null;
            }
          })
    },
    remove: function(username){
        Api.remove(username)
          .then(response => {
            if(response.error !== null){
                this.handleError(response.error);
            } else {
                // if we delete a specific searched for user go back to page 1
                if(this.username !== null) {
                    this.page = 1;
                }
                this.list();
            }
          })
    },
    handleError: function(error){
        this.error = error;
    },
    nextPage: function(){
      this.page = this.page + 1;
      this.list();
    },
    prevPage: function(){
      this.page = this.page - 1;
      this.list();
    },
    clear: function(){
      this.error = null;
      this.username = null;
      this.page = 1;
      this.list()
    },
  },
})
